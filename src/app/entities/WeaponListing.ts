
export interface WeaponListing {
  Name: string;
  AttackBonus: number;
  DamageDice: string;
  DamageBonus: number;
  DmgMult?: number;
}
