import { Component, Input, OnInit } from '@angular/core';
import { MaterialModule } from '../../material/material.module';
import { MatTableDataSource } from '@angular/material/table';
import { Attack } from '../../entities/Attack';
import { WeaponListing } from '../../entities/WeaponListing';

@Component({
  selector: 'app-ranged-attacks-shared',
  standalone: true,
  imports: [
    MaterialModule
  ],
  templateUrl: './ranged-attacks-shared.component.html',
  styleUrl: './ranged-attacks-shared.component.scss'
})
export class RangedAttacksSharedComponent implements OnInit{

  @Input() dataSource!: MatTableDataSource<WeaponListing>;
  @Input() baseAttBonus!: number;
  @Input() dexMod!: number;
  @Input() isHasted!: boolean;
  @Input() element!: string;
  @Input() maxDeckPoints!: number
  @Input() characterLevel!: number
  @Input() attackIterations!: Attack[];
  @Input() currentAttackIteration!: Attack;

  @Input() withinNineMeters: boolean = false;
  @Input() deadlyAimActive: boolean = false;

  displayedColumns: string[] = ["Arma", "Attacco", "Danni", "Bonus"];

  rangeIncrements: number = 0;
  rangePenalty: number = 2;

  @Input() temporaryAttMod!: number;
  @Input() temporaryDmgMod!: number;

  ngOnInit(): void {
    this.currentAttackIteration = this.attackIterations[0];

    let KINETIC_BLAST: WeaponListing = {
      "Name": "Ondata di ",
      "AttackBonus": 0,
      "DamageDice": "d6",
      "DamageBonus": 1
    }

    KINETIC_BLAST.Name += this.element;
    KINETIC_BLAST.DamageDice = 1 + Math.floor(this.characterLevel/2) + KINETIC_BLAST.DamageDice;
    KINETIC_BLAST.DamageBonus = 1 + Math.floor(this.characterLevel/2) + this.maxDeckPoints - this.dexMod;
    KINETIC_BLAST.AttackBonus += this.baseAttBonus;

    var weaponsList = this.dataSource.data;
    weaponsList.push(KINETIC_BLAST);
    this.dataSource.data = weaponsList;
  }

  calcAttackBonus(bonus: number){
    let deadlyAimPenalty = this.deadlyAimActive ? -2 : 0;
    let preciseShotBonus = this.withinNineMeters ? +1 : 0;
    let rangePenalty = -this.rangeIncrements * this.rangePenalty;

    return bonus
    + this.dexMod
    + deadlyAimPenalty
    + this.currentAttackIteration.AttackPenalty
    + preciseShotBonus
    + rangePenalty
    + this.temporaryAttMod;
  }

  calcDamageBonus(bonus: number){
    let deadlyAimBonus = this.deadlyAimActive ? 4 : 0;
    let preciseShotBonus = this.withinNineMeters ? +1 : 0;

    return bonus
    + this.dexMod
    + deadlyAimBonus
    + preciseShotBonus
    + this.temporaryDmgMod;
  }

  isSecondAttack(){
    return this.currentAttackIteration.AttackNumber != 1;
  }

  toggleBonus(event: any, attack: number, damage: number){
    if (!event.checked) {
      attack = attack * -1;
      damage = damage * -1;
    }
    this.dataSource.data.forEach(a => {a.AttackBonus += attack; a.DamageBonus += damage});
  }

  changeRangeIncrements(steps: number){
    this.rangeIncrements += steps;
    if (this.rangeIncrements > 0) this.withinNineMeters = false;
  }
}
